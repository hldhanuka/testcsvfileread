package TestCSVFileRead.TestCSVFileRead.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FileService {
    // Autowiring ResourceLoader for loading resources
    @Autowired
    private ResourceLoader resourceLoader;

    // Method to read a file from the resources folder
    public String readFileFromResources(
        String filename
    ) throws IOException {
        // Getting the resource using the ResourceLoader
        Resource resource = resourceLoader.getResource("classpath:" + filename);

        // Opening an InputStream to read the content of the resource
        InputStream inputStream = resource.getInputStream();

        // Creating a BufferedReader to read text from the InputStream efficiently
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        // StringBuilder to accumulate the lines read from the file
        StringBuilder stringBuilder = new StringBuilder();

        String line;

        // Reading each line from the file and appending it to the StringBuilder
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }

        // Closing the BufferedReader
        reader.close();

        // Returning the contents of the file as a string
        return stringBuilder.toString();
    }

    public List<List<String>> readCSVFileFromResources(
        String filename
    ) throws IOException {
        Resource resource = resourceLoader.getResource("classpath:" + filename);

        InputStream inputStream = resource.getInputStream();

        List<List<String>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");

                records.add(Arrays.asList(values));
            }
        }

        return records;
    }
}