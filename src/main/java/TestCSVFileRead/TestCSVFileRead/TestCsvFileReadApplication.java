package TestCSVFileRead.TestCSVFileRead;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCsvFileReadApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCsvFileReadApplication.class, args);
	}

}
