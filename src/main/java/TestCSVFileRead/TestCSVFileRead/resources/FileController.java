package TestCSVFileRead.TestCSVFileRead.resources;

import TestCSVFileRead.TestCSVFileRead.Service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class FileController {
    @Autowired
    private FileService fileService;

    // API endpoint to read a file from the resources folder
    @GetMapping("/read-text-file")
    public String readTextFile() {
        try {
            // Call the service method to read the file
            return fileService.readFileFromResources("File1.txt");
        } catch (IOException e) {
            // Print stack trace if an exception occurs
            e.printStackTrace();
            // Return an error message if reading the file fails
            return "Error occurred while reading file.";
        }
    }

    @GetMapping("/read-csv-file")
    public List<List<String>> readCsvFile() throws IOException {
        return fileService.readCSVFileFromResources("CSV.csv");
    }
}